package database;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import org.junit.Test;

import configuration.DatabaseConfiguration;

public class MySqlConnectionTests {

	@Test
	public void OpenConnection_GivenConfiguration_OpensConnection() throws FileNotFoundException, IOException {
		IDatabaseConnectionManager connectionManager = new MySqlConnectionManager(new DatabaseConfiguration());
		
		try {
			Connection connection = connectionManager.openConnection();
			connection.close();
		}
		catch(Exception e) {
			e.printStackTrace();
			fail("An exception occured opening the connection to the database");
		}
		
	}
}
