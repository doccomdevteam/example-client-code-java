package database;

import configuration.DatabaseConfiguration;
import org.junit.Test;
import schemaModels.Patient;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DatabaseOperationsManagerTests {
	
	@Test
	public void getAllPendingPatients_DataRequested_DataParsedCorrectly() throws IOException {
		IDatabaseConnectionManager connectionManager = new MySqlConnectionManager(new DatabaseConfiguration());
		IDatabaseOperationManager operations = new DatabaseOperationsManager(connectionManager);
		
		try {
			List<Patient> availablePatients =  operations.getAllPendingPatients();

			assertTrue(availablePatients.size() == 3);
		}
		catch(Exception e) {
			fail();
		}
	}
}
