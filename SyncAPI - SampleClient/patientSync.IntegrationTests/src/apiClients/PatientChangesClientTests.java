package apiClients;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import configuration.DatabaseConfiguration;
import configuration.IDatabaseConfiguration;
import database.DatabaseOperationsManager;
import database.IDatabaseConnectionManager;
import database.IDatabaseOperationManager;
import database.MySqlConnectionManager;
import exceptions.TooManyPatientsException;
import org.junit.*;

import clients.AuthenticationClient;
import clients.IAuthenticationClient;
import clients.IPatientChangesClient;
import clients.PatientChangesClient;
import requestModels.PatientChangesRequest;
import responseModels.PatientChangesResponse;
import configuration.ISyncConfiguration;
import configuration.SyncConfiguration;
import schemaModels.Patient;

public class PatientChangesClientTests {

	private ISyncConfiguration syncConfig;
	private IAuthenticationClient authClient;
	private IPatientChangesClient client;
	private static List<Patient> AvailablePatients;
	@BeforeClass
	public static void  testsSetup() throws Exception {
		IDatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
		IDatabaseConnectionManager databaseConnectionManager =	new MySqlConnectionManager(databaseConfiguration);
		IDatabaseOperationManager database = new DatabaseOperationsManager(databaseConnectionManager);

		AvailablePatients = database.getAllPendingPatients();
	}

	@Before
	public void  setUp() throws Exception {
		syncConfig = new SyncConfiguration();
		authClient = new AuthenticationClient(syncConfig);
  	 	client = new PatientChangesClient(syncConfig, authClient);
	}

	@After
	public void tearDown() throws TooManyPatientsException {
		// Delete changes from Careflow
		PatientChangesRequest request = new PatientChangesRequest();
		for (int i = 0; i < AvailablePatients.size(); i++) {

			request.addPatientToDeletions(AvailablePatients.get(i));
		}
		request.setUtcTimestamp(new Date());

		PatientChangesResponse response = client.executeRequest(request);
		if (response.isSuccessful()) {
			System.out.println("Test data was deleted successfully from Careflow System");
		}else	{
			System.out.println("Test data failed to be deleted from Careflow System");
		}
	}
	@Test
	public void postChanges_GivenEmptyCollection_ReturnsSuccessfully() throws FileNotFoundException, IOException {
		//Arrange
		PatientChangesRequest request = new PatientChangesRequest();
		request.setUtcTimestamp(new Date());

		//Act
		PatientChangesResponse response = client.executeRequest(request);

		//Assert
		assertTrue(response.isSuccessful());
		assertTrue(response.getStatusCode() == 204);
	}

	@Test
	public void postChanges_GivenValidSetOfNewPatients_PatientsAddedSuccessfully() throws FileNotFoundException, IOException, TooManyPatientsException {
		// Arrange
		PatientChangesRequest request =  new PatientChangesRequest();
		request.setUtcTimestamp(new Date());
		for (int i = 0; i < AvailablePatients.size(); i++) {

			request.addPatientToReplacements(AvailablePatients.get(i));
		}

		//Act
		PatientChangesResponse response = client.executeRequest(request);

		//Assert
		assertTrue(response.isSuccessful());
		assertTrue(response.getStatusCode() == 204);
	}

	@Test
	public void postChanges_GivenValidSetOfPatientsToDelete_PatientsAreRemovedSuccessfully() throws FileNotFoundException, IOException, TooManyPatientsException {
		// Arrange
		boolean setupWasSuccessful = SetupData();
		if (setupWasSuccessful) {
			PatientChangesRequest request = new PatientChangesRequest();

			request.setUtcTimestamp(new Date());
			for (int i = 0; i < AvailablePatients.size(); i++) {

				request.addPatientToDeletions(AvailablePatients.get(i));
			}

			//Act
			PatientChangesResponse response = client.executeRequest(request);

			//Assert
			assertTrue(response.isSuccessful());
			assertTrue(response.getStatusCode() == 204);
		}else{
			Assert.fail("Data setup failed");
		}
	}

	private boolean SetupData() throws TooManyPatientsException {
		PatientChangesRequest request =  new PatientChangesRequest();

		request.setUtcTimestamp(new Date());
		for (int i = 0; i < AvailablePatients.size(); i++) {

			request.addPatientToReplacements(AvailablePatients.get(i));
		}

		return client.executeRequest(request).isSuccessful();
	}
}
