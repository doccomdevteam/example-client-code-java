package apiClients;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import clients.AuthenticationClient;
import clients.IAuthenticationClient;
import responseModels.AccessTokenResponse;
import configuration.SyncConfiguration;

public class AuthenticationClientTests {

	@Test
	public void getAccessToken_GivenInvalidCredentials_ReturnsUnsuccessful() {
		IAuthenticationClient client = new AuthenticationClient(
				"https://testauth.careflowapp.com/clientapplication/accesstoken",
				"Invalid",
				"Invalid");
		
		AccessTokenResponse accessToken = client.getAccessToken();
		
		assertFalse(accessToken.isSuccessful());
		assertTrue(accessToken.getStatusCode() == 401);
		assertTrue(accessToken.getAccessToken() == null);
	}
	
	@Test
	public void getAccessToken_GivenValidCredentials_ReturnsAccessToken() throws Exception {
		IAuthenticationClient client = new AuthenticationClient(new SyncConfiguration());
		
		AccessTokenResponse accessToken = client.getAccessToken();
		
		assertTrue(accessToken.isSuccessful());
		assertTrue(accessToken.getStatusCode() == 200);
		assertFalse(accessToken.getAccessToken() == null);
		assertFalse(accessToken.getAccessToken().equals(""));
	}
}
