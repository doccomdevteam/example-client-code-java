/* CREATE SCHEMA */
DROP DATABASE IF EXISTS doccom_patient_sync;
CREATE DATABASE IF NOT EXISTS doccom_patient_sync;
USE doccom_patient_sync;
-- Setup a user for this database
GRANT USAGE ON *.* TO 'sampleuser'@'localhost'  IDENTIFIED BY 'Sample_pa33word';
DROP USER 'sampleuser'@'localhost';
CREATE USER 'sampleuser'@'localhost' IDENTIFIED BY 'Sample_pa33word';
GRANT ALL PRIVILEGES ON doccom_patient_sync.* TO 'sampleuser'@'localhost' IDENTIFIED BY 'Sample_pa33word';
FLUSH PRIVILEGES;

CREATE TABLE patientidentifiertype (
	PatientIdentifierTypeID INT NOT NULL,
	Name VarChar(50) NOT NULL,
	PRIMARY KEY (PatientIdentifierTypeID)
);

INSERT INTO patientidentifiertype (PatientIdentifierTypeID, Name) VALUES (1, 'NhsNumber');
INSERT INTO patientidentifiertype (PatientIdentifierTypeID, Name) VALUES (2, 'OrganisationIdentifier');
INSERT INTO patientidentifiertype (PatientIdentifierTypeID, Name) VALUES (3, 'CustomIdentifier');
INSERT INTO patientidentifiertype (PatientIdentifierTypeID, Name) VALUES (4, 'X-Ray Number');

CREATE TABLE gender (
	GenderID INT NOT NULL,
	Name VarChar(50) NOT NULL,
	PRIMARY KEY (GenderID)
);

INSERT INTO gender (GenderID, Name) VALUES (1, 'Male');
INSERT INTO gender (GenderID, Name) VALUES (2, 'Female');
INSERT INTO gender (GenderID, Name) VALUES (3, 'OtherSpecific');
INSERT INTO gender (GenderID, Name) VALUES (4, 'NotKnown');
INSERT INTO gender (GenderID, Name) VALUES (5, 'NotSpecified');

CREATE TABLE clinician (
	ClinicianID INT NOT NULL AUTO_INCREMENT,
	Title VarChar(20) NULL,
	FirstName VarChar(50) NOT NULL,
	LastName VarChar(50) NOT NULL,
	PRIMARY KEY (ClinicianID)
);

CREATE TABLE location (
	LocationID INT NOT NULL AUTO_INCREMENT,
	SiteName VarChar(100) NOT NULL,
	AreaName VarChar(100) NULL,
    BayName VarChar(20) NULL,
    BedName VarChar(20) NULL,
	PRIMARY KEY (LocationID)
);

CREATE TABLE patientupdate (
	PatientUpdateID INT NOT NULL AUTO_INCREMENT,
	DateOfBirth DateTime NOT NULL,
	DateDied DateTime NULL,
	Title VarChar(35) NULL,
	FirstName VarChar(40) NOT NULL,
	LastName VarChar(40) NOT NULL,
	GenderID INT NOT NULL,
	ClinicianID INT NULL,
	LocationID INT NULL,
    EstimatedDischargeDate DateTime NULL,
	RowProcessed BIT NOT NULL DEFAULT 0,
	TimeProcessed DateTime NULL,
	PRIMARY KEY (PatientUpdateID),
	FOREIGN KEY (GenderID) REFERENCES gender (GenderID),
	FOREIGN KEY (ClinicianID) REFERENCES clinician (ClinicianID),
	FOREIGN KEY (LocationID) REFERENCES location (LocationID)
);

CREATE TABLE patientidentifier (
  PatientIdentifierID int(11) NOT NULL AUTO_INCREMENT,
  PatientIdentifierValue varchar(200) NOT NULL,
  PatientIdentifierTypeID int(11) NOT NULL,
  PRIMARY KEY (PatientIdentifierID),
  FOREIGN KEY (PatientIdentifierTypeID) REFERENCES patientidentifiertype (PatientIdentifierTypeID) 
);

CREATE TABLE patientidentifierpatientupdate (
  PatientIdentifierPatientUpdateID int(11) NOT NULL AUTO_INCREMENT,
  PatientIdentifierID int(11) NOT NULL,
  PatientUpdateID int(11) NOT NULL,
  PRIMARY KEY (PatientIdentifierPatientUpdateID),
  FOREIGN KEY (PatientIdentifierID) REFERENCES patientidentifier (PatientIdentifierID),
  FOREIGN KEY (PatientUpdateID) REFERENCES patientupdate (PatientUpdateID)
);


-- -----------------------------------------------------
-- Table `doccom_patient_sync`.`spell`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS spell (
  SpellID INT NOT NULL AUTO_INCREMENT ,
  SpellIdentifier VARCHAR(200) NULL ,
  AdmissionDateTime DATETIME NULL ,
  DischargeDatetime DATETIME NULL ,
  UtcTimestamp DATETIME NULL ,
  RowProcessed BIT NOT NULL DEFAULT 0,
  PRIMARY KEY (`SpellID`) );

-- -----------------------------------------------------
-- Table `doccom_patient_sync`.`spellPatientIdentifier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS spellPatientIdentifier (
  SpellPatientIdentifierID INT NOT NULL AUTO_INCREMENT,
  PatientIdentifierID INT NOT NULL ,
  SpellID INT NOT NULL ,
  PRIMARY KEY (`SpellPatientIdentifierID`) ,
  FOREIGN KEY (PatientIdentifierID) REFERENCES patientidentifier (PatientIdentifierID),
  FOREIGN KEY (SpellID) REFERENCES spell (SpellID));



/* SEED DATA */
DELETE FROM patientupdate;
DELETE FROM clinician;
DELETE FROM location;
DELETE FROM patientidentifierpatientupdate;
DELETE FROM patientidentifier;

INSERT INTO clinician (Title, FirstName, LastName) VALUES ('DR', 'John', 'Smith');

SELECT @ClinicianID := LAST_INSERT_ID();

INSERT INTO location (SiteName, AreaName, BayName, BedName) VALUES ('Hospital', 'Ward', 'BayName 1','123');

SELECT @LocationID := LAST_INSERT_ID();

INSERT INTO patientupdate (	
	DateOfBirth,
	Title,
	FirstName,
	LastName,
	GenderID,
	ClinicianID,
	LocationID)
VALUES (
	'1987-06-18',
	'Miss',
	'Jane',
	'Doe',
	2,
	@ClinicianID,
	@LocationID
);

SELECT @PatientUpdateID := LAST_INSERT_ID();

INSERT INTO patientidentifier ( PatientIdentifierTypeID, PatientIdentifierValue) VALUES (1, '4010232137');
SELECT @PatientIdentifierID := LAST_INSERT_ID();
SELECT @SpellPatientIdentifierID := LAST_INSERT_ID();

INSERT INTO patientidentifierpatientupdate ( PatientIdentifierID, PatientUpdateID) VALUES (@PatientIdentifierID, @PatientUpdateID);

INSERT INTO patientidentifier ( PatientIdentifierTypeID, PatientIdentifierValue) VALUES (3, 'CustomIDForPatient');
SELECT @PatientIdentifierID := LAST_INSERT_ID();

INSERT INTO patientidentifierpatientupdate ( PatientIdentifierID, PatientUpdateID) VALUES (@PatientIdentifierID, @PatientUpdateID);

INSERT INTO location (SiteName, AreaName, BayName, BedName) VALUES ('Hospital', 'Ward', 'BayName 2','654');
SELECT @LocationID := LAST_INSERT_ID();

INSERT INTO clinician (Title, FirstName, LastName) VALUES ('DR', 'Jane', 'Smith');
SELECT @ClinicianID := LAST_INSERT_ID();

INSERT INTO patientupdate (	
	DateOfBirth,
	Title,
	FirstName,
	LastName,
	GenderID,
	ClinicianID,
	LocationID)
VALUES (
	'1987-06-18',
	'Mr',
	'Phil',
	'Space',
	1,
	null,
	@LocationID
);

SELECT @PatientUpdateID := LAST_INSERT_ID();

INSERT INTO patientidentifier ( PatientIdentifierTypeID, PatientIdentifierValue) VALUES (1, '4010232331');
SELECT @PatientIdentifierID := LAST_INSERT_ID();

INSERT INTO patientidentifierpatientupdate ( PatientIdentifierID, PatientUpdateID) VALUES (@PatientIdentifierID, @PatientUpdateID);

INSERT INTO patientupdate (
	DateOfBirth,
	Title,
	FirstName,
	LastName,
	GenderID,
	ClinicianID,
	LocationID)
VALUES (
	'1987-06-18',
	'Mr',
	'Robert',
	'Martin',
	1,
	@ClinicianID,
	null
);


SELECT @PatientUpdateID := LAST_INSERT_ID();

INSERT INTO patientidentifier ( PatientIdentifierTypeID, PatientIdentifierValue) VALUES (4, '334532');
SELECT @PatientIdentifierID := LAST_INSERT_ID();

INSERT INTO patientidentifierpatientupdate ( PatientIdentifierID, PatientUpdateID) VALUES (@PatientIdentifierID, @PatientUpdateID);

/* Spell Data */
/* Spell data for an existing patient*/
INSERT INTO spell ( SpellIdentifier, AdmissionDateTime, DischargeDatetime,UtcTimestamp) VALUES ('Spell One', DATE_ADD(NOW(),INTERVAL -1 DAY), NOW(), NOW());
SELECT @SpellID := LAST_INSERT_ID();

INSERT INTO spellpatientidentifier ( PatientIdentifierID, SpellID) VALUES (@SpellPatientIdentifierID, @SpellID);

/* Existing spell data for an existing patient merges patient identifiers*/
INSERT INTO patientidentifier ( PatientIdentifierTypeID, PatientIdentifierValue) VALUES (4, '65682');
SELECT @PatientIdentifierID := LAST_INSERT_ID();

INSERT INTO spellpatientidentifier ( PatientIdentifierID, SpellID) VALUES (@PatientIdentifierID, @SpellID);


/* SPROCS */
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TOP_50_PENDING_PATIENT_UPDATES`()
BEGIN

SELECT 	PU.PatientUpdateID,	
	PU.DateOfBirth,
	PU.DateDied,
	PU.Title,
	PU.FirstName,
	PU.LastName,
	PU.EstimatedDischargeDate,
	G.Name as Gender,
	PU.ClinicianID,
	CL.Title as ClinicianTitle,
	CL.FirstName as ClinicianFirstName,
	CL.LastName as ClinicianLastName,
	PU.LocationID,
	LO.SiteName,
	LO.AreaName,
    LO.BayName,
    LO.BedName,    
	PU.RowProcessed,
	PU.TimeProcessed
FROM patientupdate PU
INNER JOIN gender G ON G.GenderID = PU.GenderID
LEFT JOIN clinician CL ON CL.ClinicianID = PU.ClinicianID
LEFT JOIN location LO ON LO.LocationID = PU.LocationID
WHERE PU.RowProcessed = 0
LIMIT 50;

SELECT 
	*
FROM patientidentifier
INNER JOIN patientidentifiertype ON patientidentifiertype.PatientIdentifierTypeID = patientidentifier.PatientIdentifierTypeID
INNER JOIN patientidentifierpatientupdate ON patientidentifierpatientupdate.PatientIdentifierID  = patientidentifier.PatientIdentifierID
INNER JOIN patientupdate ON patientupdate.PatientUpdateID = patientidentifierpatientupdate.PatientUpdateID
WHERE patientupdate.RowProcessed = 0;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `TOP_50_PENDING_SPELL_UPDATES`()
BEGIN

SELECT 	
	*
FROM spell 
WHERE spell.RowProcessed = 0
LIMIT 50;

SELECT 
	*
FROM patientidentifier
INNER JOIN patientidentifiertype ON patientidentifiertype.PatientIdentifierTypeID = patientidentifier.PatientIdentifierTypeID
INNER JOIN spellPatientidentifier spi ON spi.PatientIdentifierID = patientidentifier.PatientIdentifierID
INNER JOIN spell  ON spell.SpellID = spi.SpellID
WHERE spell.RowProcessed = 0;

END$$
DELIMITER ;

