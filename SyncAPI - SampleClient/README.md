##SyncAPIJavaClient

###Quick Start

The `patientSync.synchronizer.Synchronizer.class` holds the entry point of this client and is a good starting place for understanding the underlying code.

To get the project up and running, you'll need to:

1. Run the scripts in the `/database` folder into MySQL to create the database and schema
2. Set up the config.properties of the `patientSync.synchronizer` project to point to the correct database and use your client credentials
3. Done!

The `Synchronizer.class` can now be scheduled to run, and will pick up any unprocessed entries in the MySQL database

**NB: Please ensure you use a testing network provided by DocCom and you have updated to configuration file `config.properties` (in the Synchronizer package) with the client credentials.**
###Overview

A Java client for synchronizing patients with the SyncAPI.

The client itself is written in Java with the JavaSE-1.7 JRE using IntelliJ or Eclipse; with the projects breaking down like so:

* patientSync.synchronizer
    * Responsible for pulling the patients out of a MySQL database and sending them to the API
    * Translates unprocessed patients into models and sends them via the apiClient
    * Marks processed patients in the database
* patientSync.apiClient
    * Responsible for co-ordinating the synchronization of patient data with the Sync API
    * Contains the http clients and models used for this communication
* patientSync.IntegrationTests
    * Allows testing of the patientSync code

###Database

The `patientSync` project is designed to use a MySQL backend database with the provided schema for sending alerts.  By default the project will run once through the contents of the table, sending the patient information in batches of 50 to the SyncAPI before marking the entries as complete in the database - providing a successful response is received.

Scripts to generate the database and schema can be found in the `/database` folder of this repository.  They are numbered in the order they should be run.

###Dependencies

The `patientSync.apiClient` project depends on the below libraries (Provided in `/PatientSync.apiClient/lib`)

* google-gson-2.2.4
    * Json serializer
    * Allows serialization of strongly typed objects without decorating classes or properties with attributes.
    * Serializes private properties by default
* httpcomponents-client-4.3.3
    * Apache library for handling http requests
    * Removes a lot of boilerplate code around sending requests

The `patientSync.synchronizer` project depends on the below library (Provided in `/PatientSync.synchronizer/lib`)

* mysql-connector-java-5.1.30-bin.jar
   * Connector/J library providing the connection to the MySQL database
   * This is wrapped in MySqlConnectionManager (Implementing IDatabaseConnectionManager) if a different driver is desired

###Integration Tests

The tests are provided to assist in integrating and debugging the code.  The majority will not run until the config.properties file within the `patientSync.IntegrationTests` project has been configured.  Please note that unlike unit tests these do call directly to the API, so please ensure you use a testing network provided by DocCom.
