package clients;

import configuration.ISyncConfiguration;
import helpers.HttpHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import requestModels.PatientChangesRequest;
import requestModels.SpellRequest;
import responseModels.AccessTokenResponse;
import responseModels.PatientChangesResponse;
import responseModels.SpellResponse;

/**
 * Responsible for handling communication with the DocCom authentication server.
 */
public class SpellsClient implements ISpellsClient {
	private String syncUrl;
	private String spellEndpoint;
	private IAuthenticationClient authClient;

	/**
	 * @param syncConfiguration Used to get the sync api url from config.properties
	 * @param authClient The client used to authenticate the user before sending the patients.
	 */
	public SpellsClient(
			ISyncConfiguration syncConfiguration,
			IAuthenticationClient authClient) {
		this.syncUrl = syncConfiguration.syncUrl();
		this.spellEndpoint = syncConfiguration.spellEndpoint();
		this.authClient = authClient;
	}
	
	/* (non-Javadoc)
	 * @see clients.IPatientChangesClient#postChanges(requestModels.PatientChangesRequest)
	 */
	public SpellResponse executeRequest(SpellRequest request) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(syncUrl + this.spellEndpoint);
		
		AccessTokenResponse accessTokenResponse = authClient.getAccessToken();
		
		if (!accessTokenResponse.isSuccessful()) {
			return SpellResponse.Failure(
					"Could not authenticate, please check your credentials are accurate.");
		}
		
		httpPost.setHeader("Authorization", accessTokenResponse.getAccessToken());
		
		try {
			HttpEntity body = HttpHelper.getJsonBodyAsEntity(request);
			httpPost.setEntity(body);
			
			HttpResponse response = httpclient.execute(httpPost);
			
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode != 201) {
				return SpellResponse.Failure(response, statusCode, "An unknown exception occurred");
			}
			
			return SpellResponse.Success(statusCode);
		} catch (Exception e) {
			return SpellResponse.Failure("An unknown exception occurred");
		}		
	}

}
