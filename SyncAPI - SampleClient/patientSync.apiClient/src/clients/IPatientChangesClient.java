package clients;

import requestModels.PatientChangesRequest;
import responseModels.PatientChangesResponse;

/**
 * Responsible for sending batches of patients to be inserted or updated
 * to the Sync API
 */
public interface IPatientChangesClient {
	
	/**
	 * Posts the provided list of patients to the Sync API
	 * @param request The list of patients to upload
	 * @return A response object indicating whether the request was successful
	 */
	PatientChangesResponse executeRequest(PatientChangesRequest request);

}