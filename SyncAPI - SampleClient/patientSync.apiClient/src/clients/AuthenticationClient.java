package clients;

import helpers.HttpHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import requestModels.ClientCredentials;
import responseModels.AccessTokenResponse;
import configuration.ISyncConfiguration;


/**
 * Responsible for handling communication with the DocCom authentication server.
 */
public class AuthenticationClient implements IAuthenticationClient {
	private String authUrl;
	private ClientCredentials clientCredentials;
	
	
	/**
	 * @param syncConfiguration Used to get the authentication URL and client credentials from config.properties
	 */
	public AuthenticationClient(ISyncConfiguration syncConfiguration) throws Exception {
		this.authUrl = syncConfiguration.authUrl();
		
		clientCredentials = new ClientCredentials(
				syncConfiguration.clientID(), 
				syncConfiguration.clientSecret());
	}
	
	/**
	 * @param authUrl The URL of the authentication server
	 * @param clientId The identifier provided by DocCom
	 * @param clientSecret The secret provided by DocCom
	 */
	public AuthenticationClient(String authUrl, String clientId, String clientSecret) {
		this.authUrl = authUrl;
		
		clientCredentials = new ClientCredentials(
				clientId, clientSecret);
	}
	
	
	/* (non-Javadoc)
	 * @see clients.IAuthenticationClient#getAccessToken()
	 */
	public AccessTokenResponse getAccessToken() {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost authPost = new HttpPost(authUrl);
		
		try {
			HttpEntity body = HttpHelper.getJsonBodyAsEntity(clientCredentials);
			authPost.setEntity(body);
			
			HttpResponse response = httpclient.execute(authPost);
			
			int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode != 200) {
				return AccessTokenResponse.Failure(statusCode);
			}
			
			String accessToken = response.getFirstHeader("access_token").getValue();
			
			return AccessTokenResponse.Success(statusCode, accessToken);
		} catch (Exception e) {
			return AccessTokenResponse.Failure();
		}		
	}
}
