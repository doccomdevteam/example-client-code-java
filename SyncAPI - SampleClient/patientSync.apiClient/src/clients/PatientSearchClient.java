package clients;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import configuration.ISyncConfiguration;
import helpers.HttpHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import requestModels.PatientChangesRequest;
import requestModels.PatientSearchRequest;
import responseModels.AccessTokenResponse;
import responseModels.PatientChangesResponse;
import responseModels.PatientSearchResponse;

import java.io.IOException;

/**
 * Responsible for handling communication with the DocCom authentication server.
 */
public class PatientSearchClient implements IPatientSearchClient {
	private String syncUrl;
	private String patientChangesEndpoint;
	private String patientSearchEndpoint;
	private IAuthenticationClient authClient;

	/**
	 * @param syncConfiguration Used to get the sync api url from config.properties
	 * @param authClient The client used to authenticate the user before sending the patients.
	 */
	public PatientSearchClient(
			ISyncConfiguration syncConfiguration,
			IAuthenticationClient authClient) {
		this.syncUrl = syncConfiguration.syncUrl();
		this.patientChangesEndpoint = syncConfiguration.patientChangesEndpoint();
		this.patientSearchEndpoint = syncConfiguration.patientSearchEndpoint();
		this.authClient = authClient;
	}
	
	/* (non-Javadoc)
	 * @see clients.IPatientChangesClient#postChanges(requestModels.PatientChangesRequest)
	 */
	public PatientSearchResponse executeRequest(PatientSearchRequest request) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost changesPost = new HttpPost(syncUrl + this.patientSearchEndpoint);
		
		AccessTokenResponse accessTokenResponse = authClient.getAccessToken();
		
		if (!accessTokenResponse.isSuccessful()) {
			return PatientSearchResponse.Failure(
					"Could not authenticate, please check your credentials are accurate.");
		}
		
		changesPost.setHeader("Authorization", accessTokenResponse.getAccessToken());
		
		try {
			HttpEntity body = HttpHelper.getJsonBodyAsEntity(request);
			changesPost.setEntity(body);
			
			HttpResponse response = httpclient.execute(changesPost);
			
			int statusCode = response.getStatusLine().getStatusCode();
			String responseBody = getResponseBody(response);
			GsonBuilder builder = new GsonBuilder();
			Gson gson = builder.create();
			JsonArray jsonArray = gson.fromJson(responseBody,JsonArray.class);
			Boolean hasResults = jsonArray.getAsJsonArray().size() > 0 ;      // get the 'user' JsonElement
			if (hasResults & statusCode != 200) {
				return PatientSearchResponse.Failure(statusCode, "An unknown exception occurred");
			}
			
			return PatientSearchResponse.Success(jsonArray,statusCode);
		} catch (Exception e) {
			return PatientSearchResponse.Failure("An unknown exception occurred");
		}		
	}

	private static String getResponseBody(HttpResponse response) throws IOException {
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			return EntityUtils.toString(entity);
		}
		return "";
	}
}
