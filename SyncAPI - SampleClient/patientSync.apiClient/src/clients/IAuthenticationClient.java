package clients;

import responseModels.AccessTokenResponse;

/**
 * Responsible for handling communication with the DocCom authentication server
 */
public interface IAuthenticationClient {
	
	/**
	 * Get an access token from the authentication server using your provided credentials.
	 * @return A response object indicating whether the request was successful
	 */
	public AccessTokenResponse getAccessToken();

}