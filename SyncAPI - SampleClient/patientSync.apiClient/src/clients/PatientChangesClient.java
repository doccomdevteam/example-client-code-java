package clients;

import configuration.ISyncConfiguration;
import helpers.HttpHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import requestModels.PatientChangesRequest;
import responseModels.AccessTokenResponse;
import responseModels.PatientChangesResponse;

/**
 * Responsible for handling communication with the DocCom authentication server.
 */
public class PatientChangesClient implements IPatientChangesClient {
	private String syncUrl;
	private String patientChangesEndpoint;
	private IAuthenticationClient authClient;
	
	/**
	 * @param syncConfiguration Used to get the sync api url from config.properties
	 * @param authClient The client used to authenticate the user before sending the patients.
	 */
	public PatientChangesClient(
			ISyncConfiguration syncConfiguration,
			IAuthenticationClient authClient) {
		this.syncUrl = syncConfiguration.syncUrl();
		this.patientChangesEndpoint = syncConfiguration.patientChangesEndpoint();
		this.authClient = authClient;
	}
	
	/* (non-Javadoc)
	 * @see clients.IPatientChangesClient#postChanges(requestModels.PatientChangesRequest)
	 */
	public PatientChangesResponse executeRequest(PatientChangesRequest request) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost changesPost = new HttpPost(syncUrl + this.patientChangesEndpoint);
		
		AccessTokenResponse accessTokenResponse = authClient.getAccessToken();
		
		if (!accessTokenResponse.isSuccessful()) {
			return PatientChangesResponse.Failure(
					"Could not authenticate, please check your credentials are accurate.");
		}
		
		changesPost.setHeader("Authorization", accessTokenResponse.getAccessToken());
		
		try {
			HttpEntity body = HttpHelper.getJsonBodyAsEntity(request);
			changesPost.setEntity(body);
			
			HttpResponse response = httpclient.execute(changesPost);
			
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode != 204) {
				return PatientChangesResponse.Failure(response, statusCode, "An unknown exception occurred");
			}
			
			return PatientChangesResponse.Success(statusCode);
		} catch (Exception e) {

			return PatientChangesResponse.Failure("An unknown exception occurred");
		}		
	}

}
