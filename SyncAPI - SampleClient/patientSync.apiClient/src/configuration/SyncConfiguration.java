package configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Responsible for pulling configuration information out of the config.properties file
 */
public class SyncConfiguration implements ISyncConfiguration {
	private static String AUTH_URL_KEY = "authurl";
	private static String SYNC_URL_KEY = "syncurl";
	private static String CLIENT_ID_KEY = "clientid";
	private static String CLIENT_SECRET_KEY = "clientsecret";
	private static String PATIENT_CHANGES_URL_KEY = "patientchangesendpoint";
	private static String PATIENT_SEARCH_URL_KEY = "patientsearchendpoint";
	private static String SPELL_URL_KEY = "spellendpoint";
	
	private ResourceBundle prop;
	
	/**
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public SyncConfiguration() throws FileNotFoundException, IOException {
		prop = ResourceBundle.getBundle("config");
	}
	
	/* (non-Javadoc)
	 * @see configuration.ISyncConfiguration#authUrl()
	 */
	public String authUrl() {
		return prop.getString(AUTH_URL_KEY);
	}
	
	/* (non-Javadoc)
	 * @see configuration.ISyncConfiguration#syncUrl()
	 */
	public String syncUrl() {
		return prop.getString(SYNC_URL_KEY);
	}
	
	/* (non-Javadoc)
	 * @see configuration.ISyncConfiguration#clientID()
	 */
	public String clientID() throws IllegalArgumentException {
		String clientId = prop.getString(CLIENT_ID_KEY);
		if ( clientId.equalsIgnoreCase("xxxx")) {
			throw new IllegalArgumentException("The CLIENT_ID_KEY has not been set. Please update th configuration file and try again");
		}
		return clientId;
	}
	
	/* (non-Javadoc)
	 * @see configuration.ISyncConfiguration#clientSecret()
	 */
	public String clientSecret() throws IllegalArgumentException {
		String clientSecret = prop.getString(CLIENT_SECRET_KEY);
		if ( clientSecret.equalsIgnoreCase("xxxx")) {
			throw new IllegalArgumentException("The CLIENT_ID_KEY has not been set. Please update th configuration file and try again");
		}
		return clientSecret;
	}

	/**
	 * @return The part of url for the Changes endpoint
	 */
	public String patientChangesEndpoint() {
		return prop.getString(PATIENT_CHANGES_URL_KEY);
	}

	/**
	 * @return The part of url for the Search endpoint
	 */
	public String patientSearchEndpoint() {
		return prop.getString(PATIENT_SEARCH_URL_KEY);
	}

	/**
	 * @return The part of url for the Spell endpoint
	 */
	public String spellEndpoint() {
		return prop.getString(SPELL_URL_KEY);
	}
}
