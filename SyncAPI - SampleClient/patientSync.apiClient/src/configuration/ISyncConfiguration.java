package configuration;

/**
 * Responsible for pulling configuration information out of the config.properties file
 */
public interface ISyncConfiguration {
	
	/**
	 * The URL of the authentication server, must include the targeted resource
	 * eg "https://auth.com/clientapplication/accesstoken"
	 * @return The URL of the authentication server
	 */
	String authUrl();
	
	/**
	 * The URL of the Sync API; just the base URL with a trailing slash
	 * eg "https://sync.com/"
	 * @return The URL of the Sync API
	 */
	String syncUrl();
	
	/**
	 * The identifier of the client provided by DocCom for authentication
	 * @return The client id
	 */
	String clientID() throws Exception;
	
	/**
	 * The secret for authentication provided by DocCom
	 * @return The client secret
	 */
	String clientSecret() throws Exception;

	/**
	 * @return The part of url for the Changes endpoint
	 */
	String patientChangesEndpoint();

	/**
	 * @return The part of url for the Search endpoint
	 */
	String patientSearchEndpoint();

	/**
	 * @return The part of url for the Spell endpoint
	 */
	String spellEndpoint();
}