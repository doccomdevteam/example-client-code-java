package schemaModels;

/**
 * The type of unique identifier a patient can be sent with.
 * This maps to the currently supported types of identifier.
 */
public enum PatientIdentifierType {
	NhsNumber, 
	OrganisationIdentifier
}