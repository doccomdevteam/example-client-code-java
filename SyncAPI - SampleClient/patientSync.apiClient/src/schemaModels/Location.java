package schemaModels;

/**
 * The location object residing on a patient being uploaded to the Sync API.
 * This represents the location the patient currently resides in.
 * Site is the parent location, for example a hospital
 * Area is a setting inside the location, for example a ward
 * Bay is a location with a location, for example Bay 1 in a ward
 * Bed is the bed name or number within a bay, for example 1234 in Bay 1
 */
public class Location {
	private String siteName;
	private String areaName;
	private String bay;
	private String bed;

	/**
	 * Gets the name of the site the patient resides in
	 * @return The name of the site
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * Sets the name of the site the patient resides in
	 * eg "A Hospital"
	 * @param siteName The name of the site
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * Gets the name of the area the patient resides in
	 * @return The name of the area
	 */
	public String getAreaName() {
		return areaName;
	}

	/**
	 * Sets the name of the area the patient resides in
	 * eg: "A Ward"
	 * @param areaName The name of the area
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}


	/**
	 * Gets the name of the bay  the patient resides in
	 * @return the name of the bay
	 */
	public String getBay() {
		return bay;
	}

	/**
	 * Sets the name of the bay  the patient resides in
	 * @param bay the name of the bay
	 */
	public void setBay(String bay) {
		this.bay = bay;
	}

	/**
	 * Gets the name of the bed  the patient resides in
	 * @return the name of the bed
	 */
	public String getBed() {
		return bed;
	}

	/**
	 * Sets the name of the bed  the patient resides in
	 * @param bed the name of the bed
	 */
	public void setBed(String bed) {
		this.bed = bed;
	}
}
