package schemaModels;

import helpers.DateFormatHelper;

import java.util.Date;

/**
 * An object containing all the details used to identify a patient.
 */
public class PatientIdentifier {
	private String patientIdentifierType;
	private String patientIdentifierValue;

	/**
	 * Gets the type of unique identifier that is being used for the patient.
	 * @return The type of identifier being used.
	 */
	public String getPatientIdentifierType() {
		return patientIdentifierType;
	}
	
	/**
	 * Sets the type of unique identifier that is being used for the patient.
	 * eg: "NhsNumber" or "OrganisationIdentifier"
	 * @param patientIdentifierTypeName The type of identifier.
	 */
	public void setPatientIdentifierType(String patientIdentifierTypeName) {
		this.patientIdentifierType = patientIdentifierTypeName;
	}
	
	/**
	 * Gets the unique identifier for the patient.
	 * @return The unique identifier for the patient.
	 */
	public String getPatientIdentifierValue() {
		return patientIdentifierValue;
	}
	
	/**
	 * Sets the unique identifier for the patient.
	 * eg: "12345"
	 * @param patientIdentifierValue The unique identifier.
	 */
	public void setPatientIdentifierValue(String patientIdentifierValue) {
		this.patientIdentifierValue = patientIdentifierValue;
	}
}
