package schemaModels;

import helpers.DateFormatHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A single patient being uploaded to the Sync API to either be inserted into the
 * population service or to replace a matching record.
 */
public class Patient {
	private List<PatientIdentifier> patientIdentifiers = new ArrayList<>();
	private String dateOfBirth;
	private String title;
	private String firstName;
	private String lastName;
	private Gender gender;
	private String dateDied;
	private String estimatedDischargeDate;
	private transient int clientIdentifier;
	private Clinician clinician;
	private Location location;


	public Patient() {
	}

	/**
	 * Gets an object containing all the details identifying the patient.
	 * @return An object containing the details that identify a patient.
	 */
	public List<PatientIdentifier> getPatientIdentifiers() {
		return patientIdentifiers;
	}

	/**
	 * Gets the patient's date of birth in the format it is sent to the API (yyyy-MM-dd)
	 * @return The patient's date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets the patient's date of birth using the format the API expects (yyyy-MM-dd)
	 * eg: "1987-06-18"
	 * @param dateOfBirth The patient's date of birth
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = DateFormatHelper.getIsoDateOnlyFormatString(dateOfBirth);
	}

	/**
	 * Sets the date the patient died using the format the API expects (yyyy-MM-dd)
	 * eg: "1987-06-18"
	 * @param dateDied The date the patient died
	 */
	public void setDateDied(Date dateDied) {
		this.dateDied = DateFormatHelper.getIsoDateOnlyFormatString(dateDied);
	}

	/**
	 * Gets the patient's first name
	 * @return The patient's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the patient's first name
	 * eg: "John"
	 * @param firstName The patient's first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the gender of the patient
	 * @return The type of gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Sets the gender of the patient
	 * eg: "Male", "Female", "NotSpecified", "NotKnown" or "OtherSpecific"
	 * @param gender The type of gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * Get's the patient's last name
	 * @return The patient's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the patient's last name
	 * eg "Smith"
	 * @param lastName The patient's last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Get's the patient's title
	 * @return The patient's title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the patient's title
	 * eg: "Mr"
	 * @param title The patient's title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Sets an object containing all the details identifying the patient.
	 * @param patientIdentifier An object containing the details that identify a patient.
	 */
	public void setPatientIdentifiers(PatientIdentifier patientIdentifier) {
		this.patientIdentifiers.add(patientIdentifier);
	}
	/**
	 * Gets an object containing the details of the clinician responsible for the patient.
	 * @return The clinician responsible for the patient.
	 */
	public Clinician getClinician() {
		return clinician;
	}
	
	/**
	 * Sets an object containing all the details of the clinician responsible for the patient.
	 * @param clinician The clinician responsible for the patient.
	 */
	public void setClinician(Clinician clinician) {
		this.clinician = clinician;
	}
	
	/**
	 * Gets an object containing the details of the location the patient currently resides in.
	 * @return The location the patient currently resides in.
	 */
	public Location getLocation() {
		return location;
	}
	
	/**
	 * Sets an object containing the details of the location the patient currently resides in.
	 * @param location The location the patient currently resides in.
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * Gets the identifier used by the client to track this patient.
	 * Used to mark the patient as processed on the client side
	 * Note: This is not serialized into the request
	 * @return The client identifier
	 */
	public int getClientIdentifier() {
		return clientIdentifier;
	}

	/**
	 * Sets the identifier used by the client to track this patient.
	 * Used to mark the patient as processed on the client side
	 * Note: This is not serialized into the request
	 * @param clientIdentifier The client identifier to set
	 */
	public void setClientIdentifier(int clientIdentifier) {
		this.clientIdentifier = clientIdentifier;
	}

	public void setEstimatedDischargeDate(java.sql.Date estimatedDischargeDate) {
		this.estimatedDischargeDate = DateFormatHelper.getIsoDateOnlyFormatString(estimatedDischargeDate);
	}
}
