package schemaModels;

public enum Gender {
	Male, 
	Female, 
	OtherSpecific, 
	NotKnown, 
	NotSpecified
}
