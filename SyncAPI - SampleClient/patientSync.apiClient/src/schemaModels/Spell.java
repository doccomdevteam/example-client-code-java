package schemaModels;

import helpers.DateFormatHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The clinician object residing on a patient being uploaded to the Sync API.
 * This represents the clinician responsible for the patient at this time.
 */
public class Spell {
	private ArrayList<PatientIdentifier> patientIdentifiers = new ArrayList<>();
	private String spellIdentifier = "";
	private Date admissionDateTime;
	private Date dischargeDatetime;
	private String utcTimestamp;
	private transient int clientIdentifier;

	public ArrayList<PatientIdentifier> getPatientIdentifiers() {
		return patientIdentifiers;
	}

	public void setPatientIdentifiers(ArrayList<PatientIdentifier> patientIdentifiers) {
		this.patientIdentifiers = patientIdentifiers;
	}

	public void addPatientIdentifier(PatientIdentifier patientIdentifier) {
		this.patientIdentifiers.add(patientIdentifier);
	}

	public String getSpellIdentifier() {
		return spellIdentifier;
	}

	public void setSpellIdentifier(String spellIdentifier) {
		this.spellIdentifier = spellIdentifier;
	}

	public Date getAdmissionDateTime() {
		return admissionDateTime;
	}

	public void setAdmissionDateTime(Date admissionDateTime) {
		this.admissionDateTime = admissionDateTime;
	}

	public Date getDischargeDatetime() {
		return dischargeDatetime;
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		this.dischargeDatetime = dischargeDatetime;
	}

	public String getUtcTimestamp() {
		return utcTimestamp;
	}

	public void setUtcTimestamp(Date utcTimestamp) {
		this.utcTimestamp = DateFormatHelper.getIsoFullPrecisionFormatString(utcTimestamp);
	}

	/**
	 * Gets the identifier used by the client to track this spell.
	 * Used to mark the spell as processed on the client side
	 * Note: This is not serialized into the request
	 * @return The client identifier
	 */
	public int getClientIdentifier() {
		return clientIdentifier;
	}

	/**
	 * Sets the identifier used by the client to track this spell.
	 * Used to mark the spell as processed on the client side
	 * Note: This is not serialized into the request
	 * @param clientIdentifier The client identifier to set
	 */
	public void setClientIdentifier(int clientIdentifier) {
		this.clientIdentifier = clientIdentifier;
	}
}