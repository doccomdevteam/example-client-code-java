package schemaModels;

/**
 * The clinician object residing on a patient being uploaded to the Sync API.
 * This represents the clinician responsible for the patient at this time.
 */
public class Clinician {
	private String title;
	private String firstName;
	private String lastName;

	/**
	 * Gets the clincian's title
	 * @return The clinician's title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the clinicians's title, eg "DR"
	 * @param title The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the clinician's first name, eg "John"
	 * @return The clinician's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the clinician's first name, eg "John"
	 * @param firstName The first name to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the clinician's last name
	 * @return The clinician's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the clinician's last name, eg "Smith"
	 * @param lastName The last name to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}