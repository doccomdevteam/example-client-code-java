package responseModels;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Contains the details returned by a request to the /patient/changes end point
 * Can be used to determine whether the request was successful
 */
public class SpellResponse {
	private boolean successful;
	private int statusCode;
	private String reason;

	private SpellResponse(){
		
	}
	
	/**
	 * Build a response object indicating the request has failed.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static SpellResponse Failure(String reason) {
		SpellResponse response = new SpellResponse();

		response.successful = false;
		response.reason = reason;
		
		return response;
	}

	private static String getResponseBody(HttpResponse response) throws IOException {
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			return EntityUtils.toString(entity);
		}
		return "";
	}
	/**
	 * Build a response object indicating the request has failed.
	 * @param statusCode The status code returned by the request.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static SpellResponse Failure(HttpResponse httpResponse, int statusCode, String reason) throws IOException {
		SpellResponse response = new SpellResponse();

		// Extract reason for failure
/*		String responseBody = getResponseBody(httpResponse);
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		JsonObject jsonObject = gson.fromJson(responseBody,JsonObject.class);
		String name = jsonObject.getAsJsonObject("errors").getAsString() ;      // get the 'user' JsonElement*/

		response.successful = false;
		response.statusCode = statusCode;
		response.reason = reason;
		
		return response;
	}
	
	/**
	 * Builds a response object indicating the request has succeeded.
	 * @param statusCode The status code returned by the request.
	 * @return An object indicated the request has succeeded.
	 */
	public static SpellResponse Success(int statusCode) {
		SpellResponse response = new SpellResponse();
		
		response.successful = true;
		response.statusCode = statusCode;
		
		return response;
	}

	/**
	 * Returns a value indicating whether the response was successful.
	 * @return True if the response was successful, otherwise false.
	 */
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 * Returns the reason the request failed.  Only set if the request was not successful.
	 * @return The reason the request failed.
	 */
	public String getFailureReason() {
		return reason;
	}
	
	/**
	 * Returns the status code the authentication server sent back in the response.
	 * @return The status code from the authentication server.
	 */
	public int getStatusCode() {
		return statusCode;
	}
}
