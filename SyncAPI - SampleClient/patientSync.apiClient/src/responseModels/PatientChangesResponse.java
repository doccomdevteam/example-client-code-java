package responseModels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import helpers.HttpHelper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Contains the details returned by a request to the /patient/changes end point
 * Can be used to determine whether the request was successful
 */
public class PatientChangesResponse {
	private boolean successful;
	private int statusCode;
	private String reason;
	
	private PatientChangesResponse(){
		
	}
	
	/**
	 * Build a response object indicating the request has failed.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static PatientChangesResponse Failure(String reason) {
		PatientChangesResponse response = new PatientChangesResponse();

		response.successful = false;
		response.reason = reason;
		
		return response;
	}

	private static String getResponseBody(HttpResponse response) throws IOException {
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			return EntityUtils.toString(entity);
		}
		return "";
	}
	/**
	 * Build a response object indicating the request has failed.
	 * @param statusCode The status code returned by the request.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static PatientChangesResponse Failure(HttpResponse httpResponse, int statusCode, String reason) throws IOException {
		PatientChangesResponse response = new PatientChangesResponse();

		// Extract reason for failure
/*		String responseBody = getResponseBody(httpResponse);
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		JsonObject jsonObject = gson.fromJson(responseBody,JsonObject.class);
		String name = jsonObject.getAsJsonObject("errors").getAsString() ;      // get the 'user' JsonElement*/

		response.successful = false;
		response.statusCode = statusCode;
		response.reason = reason;
		
		return response;
	}
	
	/**
	 * Builds a response object indicating the request has succeeded.
	 * @param statusCode The status code returned by the request.
	 * @return An object indicated the request has succeeded.
	 */
	public static PatientChangesResponse Success(int statusCode) {
		PatientChangesResponse response = new PatientChangesResponse();
		
		response.successful = true;
		response.statusCode = statusCode;
		
		return response;
	}

	/**
	 * Returns a value indicating whether the response was successful.
	 * @return True if the response was successful, otherwise false.
	 */
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 * Returns the reason the request failed.  Only set if the request was not successful.
	 * @return The reason the request failed.
	 */
	public String getFailureReason() {
		return reason;
	}
	
	/**
	 * Returns the status code the authentication server sent back in the response.
	 * @return The status code from the authentication server.
	 */
	public int getStatusCode() {
		return statusCode;
	}
}
