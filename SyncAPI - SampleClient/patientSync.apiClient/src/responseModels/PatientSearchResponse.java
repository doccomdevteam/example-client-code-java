package responseModels;

import com.google.gson.JsonArray;

/**
 * Contains the details returned by a request to the /patient/changes end point
 * Can be used to determine whether the request was successful
 */
public class PatientSearchResponse {
	private boolean successful;
	private int statusCode;
	private JsonArray body;
	private String reason;

	private PatientSearchResponse(){
		
	}
	
	/**
	 * Build a response object indicating the request has failed.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static PatientSearchResponse Failure(String reason) {
		PatientSearchResponse response = new PatientSearchResponse();
		
		response.successful = false;
		response.reason = reason;
		
		return response;
	}
	
	/**
	 * Build a response object indicating the request has failed.
	 * @param statusCode The status code returned by the request.
	 * @param reason The reason the request failed.
	 * @return An object indicating the request has failed.
	 */
	public static PatientSearchResponse Failure(int statusCode, String reason) {
		PatientSearchResponse response = new PatientSearchResponse();
		
		response.successful = false;
		response.statusCode = statusCode;
		response.reason = reason;
		
		return response;
	}
	
	/**
	 * Builds a response object indicating the request has succeeded.
	 * @param statusCode The status code returned by the request.
	 * @return An object indicated the request has succeeded.
	 */
	public static PatientSearchResponse Success(JsonArray jsonArray,int statusCode) {
		PatientSearchResponse response = new PatientSearchResponse();
		
		response.successful = true;
		response.statusCode = statusCode;
		response.body = jsonArray;
		return response;
	}

	/**
	 * Returns a value indicating whether the response was successful.
	 * @return True if the response was successful, otherwise false.
	 */
	public boolean isSuccessful() {
		return successful;
	}
	
	/**
	 * Returns the reason the request failed.  Only set if the request was not successful.
	 * @return The reason the request failed.
	 */
	public String getFailureReason() {
		return reason;
	}
	
	/**
	 * Returns the status code the authentication server sent back in the response.
	 * @return The status code from the authentication server.
	 */
	public int getStatusCode() {
		return statusCode;
	}

	public JsonArray getBody() {
		return body;
	}
}
