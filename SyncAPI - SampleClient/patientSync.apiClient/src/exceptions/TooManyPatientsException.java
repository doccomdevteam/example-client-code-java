package exceptions;

/**
 * Exception thrown when more that 50 patients have been added to the PatientChangesRequest object.
 * Our server only supports a maximum of 50 patients in the replacement and deletion collections.
 * Each collection supports up to 50 independent of the other
 * (so 45 replacements and 40 deletions is perfectly valid)
 */
public class TooManyPatientsException extends Exception {
	private static final long serialVersionUID = -4130168742841988674L;
	
	/**
	 Exception thrown when more that 50 patients have been added to the PatientChangesRequest object.
	 * Our server only supports a maximum of 50 patients in the replacement and deletion collections.
	 * Each collection supports up to 50 independent of the other
	 * (so 45 replacements and 40 deletions is perfectly valid)
	 */
	public TooManyPatientsException() {
        super("A maximum of 50 patients at a time can be replaced or deleted");
    }

}
