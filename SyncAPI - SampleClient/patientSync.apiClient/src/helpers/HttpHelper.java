package helpers;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Static class responsible for performing logic common between the api client classes
 */
public final class HttpHelper {
	private static GsonBuilder builder;
	
	static {
		builder = new GsonBuilder();
		builder.disableHtmlEscaping();
	}
	
	/**
	 * Converts a provided object into a json string then builds a HttpEntity
	 * object as expected by the apache http code.
	 * @param source The object to convert into a json string
	 * @return A HttpEntity object as used by the http code to perform a request.
	 * @throws UnsupportedEncodingException
	 */
	public static HttpEntity getJsonBodyAsEntity(Object source) throws UnsupportedEncodingException {		
		Gson gson = builder.create();
		String jsonBody = gson.toJson(source);
		StringEntity entity = new StringEntity(jsonBody);
		entity.setContentType("application/json");
		return entity;
	}
	
	/**
	 * Converts a provided object into a json string; used for testing serialization
	 * @param source The object to convert into a json string
	 * @return The JSON string
	 * @throws UnsupportedEncodingException
	 */
	public static String getRawJsonBody(Object source) throws UnsupportedEncodingException {		
		Gson gson = builder.create();
		return gson.toJson(source);
	}
}
