package helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Static class responsible for formatting dates so they are sent to the API
 * in the expected format
 */
public final class DateFormatHelper {
	private static SimpleDateFormat isoDateOnlyFormat;
	private static SimpleDateFormat isoFullPrecisionFormat;
	
	static {
		isoDateOnlyFormat = new SimpleDateFormat("yyyy-MM-dd");
		isoFullPrecisionFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
	}
	
	/**
	 * Returns the given date in the format "yyyy-MM-dd"
	 * @param sourceDate The date to convert
	 * @return The date in date only format
	 */
	public static String getIsoDateOnlyFormatString(Date sourceDate) {
		return isoDateOnlyFormat.format(sourceDate);
	}
	
	/**
	 * Returns the given date in the format "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'"
	 * This is the format we expect timestamps in on the API.
	 * @param sourceDate The date to convert
	 * @return The date in full precision format
	 */
	public static String getIsoFullPrecisionFormatString(Date sourceDate) {
		return isoFullPrecisionFormat.format(sourceDate);
	}
}
