package requestModels;

import helpers.DateFormatHelper;

import java.util.ArrayList;
import java.util.Date;

import schemaModels.Patient;
import exceptions.TooManyPatientsException;
import schemaModels.PatientIdentifier;

/**
 * Strongly typed version of the JSON expected by the /patient/changes end point of the Sync API.
 */
public class PatientChangesRequest {
	private ArrayList<Patient> replacements = new ArrayList<>();
	private ArrayList<PatientIdentifier> deletions = new ArrayList<>();
	private String utcTimestamp;
	
	/**
	 * Adds a patient to the replacements collection to be sent to the API
	 * @param patientToAdd The patient to add
	 * @throws TooManyPatientsException Thrown when there are over 50 patients in the collection (Max supported)
	 */
	public void addPatientToReplacements(Patient patientToAdd) throws TooManyPatientsException {
		if (replacements.size() >= 50) {
			throw new TooManyPatientsException();
		}
		
		replacements.add(patientToAdd);
	}

	/**
	 * Adds a patient to the deletions collection to be sent to the API
	 * @param patientToDelete
	 * @throws TooManyPatientsException
	 */
	public void addPatientToDeletions(Patient patientToDelete) throws TooManyPatientsException{
		if (deletions.size() >= 50) {
			throw new TooManyPatientsException();
		}

		deletions.add(patientToDelete.getPatientIdentifiers().get(0));
	}
	/**
	 * Gets the total number of patients being replaced in this request
	 * @return The total number of patients being replaced in this request
	 */
	public int getNumberOfPatientsBeingReplaced() {
		return replacements.size();
	}
	
	/**
	 * Gets the list of patients in the replacements collection
	 * @return The patients in the replacement collection
	 */
	public ArrayList<Patient> getPatientReplacements() {
		return replacements;
	}

	/**
	 * Returns the UTC time stamp in the format it is sent to the API (Full precision ISO 8601)
	 * @return The UTC time stamp in string format
	 */
	public String getUtcTimestamp() {
		return utcTimestamp;
	}
	
	/**
	 * Sets the UTC time stamp; converting the date into the correct format ISO 8601 string
	 * @param utcTimestamp The date to convert into a time stamp
	 */
	public void setUtcTimestamp(Date utcTimestamp) {
		this.utcTimestamp = DateFormatHelper.getIsoFullPrecisionFormatString(utcTimestamp);
	}
}
