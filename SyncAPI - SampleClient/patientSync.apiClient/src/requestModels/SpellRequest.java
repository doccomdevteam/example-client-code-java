package requestModels;

import helpers.DateFormatHelper;
import schemaModels.PatientIdentifier;
import schemaModels.Spell;

import java.util.ArrayList;
import java.util.Date;

/**
 * Strongly typed version of the JSON expected by the /patient/changes end point of the Sync API.
 */
public class SpellRequest {
	private ArrayList<PatientIdentifier> patientIdentifiers = new ArrayList<>();
	private String spellIdentifier = "";
	private String admissionDateTime;
	private String dischargeDatetime;
	private String utcTimestamp;

	public SpellRequest(Spell spell){
		patientIdentifiers = spell.getPatientIdentifiers();
		spellIdentifier = spell.getSpellIdentifier();
		admissionDateTime = DateFormatHelper.getIsoFullPrecisionFormatString(spell.getAdmissionDateTime());
		dischargeDatetime = DateFormatHelper.getIsoFullPrecisionFormatString(spell.getDischargeDatetime());
		utcTimestamp = spell.getUtcTimestamp();
	}
	public ArrayList<PatientIdentifier> getPatientIdentifiers() {
		return patientIdentifiers;
	}

	public void setPatientIdentifiers(ArrayList<PatientIdentifier> patientIdentifiers) {
		this.patientIdentifiers = patientIdentifiers;
	}

	public String getSpellIdentifier() {
		return spellIdentifier;
	}

	public void setSpellIdentifier(String spellIdentifier) {
		this.spellIdentifier = spellIdentifier;
	}

	public String getAdmissionDateTime() {
		return admissionDateTime;
	}

	public void setAdmissionDateTime(Date admissionDateTime) {
		this.admissionDateTime = DateFormatHelper.getIsoFullPrecisionFormatString(admissionDateTime);
	}

	public String getDischargeDatetime() {
		return dischargeDatetime;
	}

	public void setDischargeDatetime(Date dischargeDatetime) {
		this.dischargeDatetime = DateFormatHelper.getIsoFullPrecisionFormatString(dischargeDatetime);
	}

	public String getUtcTimestamp() {
		return utcTimestamp;
	}

	public void setUtcTimestamp(Date utcTimestamp) {
		this.utcTimestamp = DateFormatHelper.getIsoFullPrecisionFormatString(utcTimestamp);
	}
}
