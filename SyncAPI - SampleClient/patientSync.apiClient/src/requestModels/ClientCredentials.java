package requestModels;

/**
 * Holds the credentials of the client accessing the Sync API
 * These are used to authenticate the client before allowing any requests
 */
public class ClientCredentials {
	private String clientID;
	private String clientSecret;
	
	/**
	 * Construct the client credentials object with the required fields.
	 * @param clientID The client identifier as provided by DocCom.
	 * @param clientSecret The client secret as provided by DocCom.
	 */
	public ClientCredentials(String clientID, String clientSecret) {
		this.clientID = clientID;
		this.clientSecret = clientSecret;
	}
	
	/**
	 * Gets the client identifier stored in this object
	 * @return The client identifier.
	 */
	public String getClientID() {
		return clientID;
	}
	
	/**
	 * Gets the client secret stored in this object
	 * @return The client secret
	 */
	public String getClientSecret() {
		return clientSecret;
	}
}
