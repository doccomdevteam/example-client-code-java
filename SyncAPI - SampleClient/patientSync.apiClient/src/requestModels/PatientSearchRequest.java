package requestModels;

import exceptions.TooManyPatientsException;
import helpers.DateFormatHelper;
import schemaModels.Patient;

import java.util.ArrayList;
import java.util.Date;

/**
 * Strongly typed version of the JSON expected by the /patient/changes end point of the Sync API.
 */
public class PatientSearchRequest {
	private String IdentifierType = "";
	private String IdentifierValue = "";

	public void setIdentifierType(String identifierType) {
		this.IdentifierType = identifierType;
	}

	public void setIdentfierValue(String identfierValue) {
		this.IdentifierValue = identfierValue;
	}

	public String getIdentfierValue() {
		return IdentifierValue;
	}

	public String getIdentifierType() {
		return IdentifierType;
	}
	/*
		{
			"identifierType": "X-Ray Number",
			"identifierValue": "334532"
		}
	*/

}
