package database;

import java.sql.Connection;
import java.sql.SQLException;

/**
* Responsible for opening a connection to a database using the Connector/J drivers
*/
public interface IDatabaseConnectionManager {

	/**
	 * Opens a new connection to the database and returns it
	 * @return The opened connection
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Connection openConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException;

}