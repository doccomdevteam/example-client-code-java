package database;

import requestModels.PatientChangesRequest;
import schemaModels.Patient;
import schemaModels.Spell;

import java.util.List;

/**
 * Responsible for performing operations against the database
 */
public interface IDatabaseOperationManager {

	/**
	 * Gets all the patients pending synchronization with the population service
	 * @return The request object ready for sending
	 * @throws Exception 
	 */
	List<Patient> getAllPendingPatients() throws Exception;

	/**
	 * Gets all the spells pending synchronization with the population service
	 * @return The request object ready for sending
	 * @throws Exception
	 */
	List<Spell> getAllPendingSpells() throws Exception;

	/**
	 * Marks the patients in the provided object as processed in the database.
	 * @param request The request containing the patients to mark as processed
	 * @throws Exception
	 */
	public void markPatientsAsProcessed(PatientChangesRequest request) throws Exception;

}