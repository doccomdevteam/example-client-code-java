package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelTranslators.PatientChangesRequestTranslator;
import modelTranslators.SpellsRequestTranslator;
import requestModels.PatientChangesRequest;
import schemaModels.Patient;
import schemaModels.PatientIdentifier;
import schemaModels.Spell;

/**
 * Responsible for performing operations against the database
 */
public class DatabaseOperationsManager implements IDatabaseOperationManager {
	private IDatabaseConnectionManager connectionManager;
	
	/**
	 * Initialises the operations class
	 * @param connectionManager Opens the connection to the database
	 */
	public DatabaseOperationsManager(IDatabaseConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}
	
	/* (non-Javadoc)
	 * @see database.IDatabaseOperations#getAllPendingPatients()
	 */
	public List<Patient> getAllPendingPatients() throws Exception {
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		List<Patient>  patientList = new ArrayList<>();
		HashMap<Integer, Patient> translatedPatients = new HashMap<>();
		try
		{
			conn = connectionManager.openConnection();
			stmt = conn.prepareCall("{CALL `doccom_patient_sync`.`TOP_50_PENDING_PATIENT_UPDATES`}");
			boolean results = stmt.execute();

			while (results) {
				rs = stmt.getResultSet();

				translatedPatients = PatientChangesRequestTranslator.translate(rs);

				rs.close();

				results = stmt.getMoreResults();
				while (results) {
					// Assign patient identifiers
					rs = stmt.getResultSet();
					while(rs.next()) {
						PatientIdentifier patientIdentifier =  PatientChangesRequestTranslator.translateIdentifier(rs);
						Patient patient = translatedPatients.get(rs.getInt("PatientUpdateID"));
						patient.setPatientIdentifiers(patientIdentifier);
					}
					rs.close();
					results = false;
				}
			}
			for (Map.Entry<Integer, Patient> entry : translatedPatients.entrySet()) {
				patientList.add(entry.getValue());
			}
			return patientList;
		}
		finally {
		    closeConnection(stmt, rs, conn);
		}
	}

	public List<Spell> getAllPendingSpells() throws  Exception{
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		List<Spell>  spellList = new ArrayList<>();
		HashMap<Integer, Spell> translatedSpells = new HashMap<>();
		try
		{
			conn = connectionManager.openConnection();
			stmt = conn.prepareCall("{CALL `doccom_patient_sync`.`TOP_50_PENDING_SPELL_UPDATES`}");
			boolean results = stmt.execute();

			while (results) {
				rs = stmt.getResultSet();

				translatedSpells = SpellsRequestTranslator.translate(rs);

				rs.close();

				results = stmt.getMoreResults();
				while (results) {
					// Assign spell identifiers
					rs = stmt.getResultSet();
					while(rs.next()) {
                        PatientIdentifier spellIdentifier =  SpellsRequestTranslator.translateIdentifier(rs);
						Spell spell = translatedSpells.get(rs.getInt("SpellID"));
						spell.addPatientIdentifier(spellIdentifier);
					}
					rs.close();
					results = false;
				}
			}
			for (Map.Entry<Integer, Spell> entry : translatedSpells.entrySet()) {
				spellList.add(entry.getValue());
			}
			return spellList;
		}
		finally {
			closeConnection(stmt, rs, conn);
		}
	}
	/* (non-Javadoc)
	 * @see database.IDatabaseOperationManager#markPatientsAsProcessed(requestModels.PatientChangesRequest)
	 */
	public void markPatientsAsProcessed(PatientChangesRequest request) throws Exception {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try
		{
			if (request.getNumberOfPatientsBeingReplaced() == 0) {
				throw new Exception("No patients provide to mark as processed");
			}
			conn = connectionManager.openConnection();
			stmt = conn.createStatement();
			String sql = buildMarkAsProcessedSql(request);
			
			int rowsAffected = stmt.executeUpdate(sql);
			
			if (rowsAffected != request.getNumberOfPatientsBeingReplaced()) {
				throw new Exception(
						"The number of patients update in the database does not match the "+
						"number of patients that were sent for replacement");
			}
		}
		finally {
		    closeConnection(stmt, rs, conn);
		}
	}

	private String buildMarkAsProcessedSql(PatientChangesRequest request) {
		String sql = 
				"UPDATE PatientUpdate " + 
				"SET RowProcessed = 1, " +
					"TimeProcessed = UTC_TIMESTAMP() " +
				"WHERE PatientUpdateID IN (";
		
		for (Patient patient : request.getPatientReplacements()) {
			sql += patient.getClientIdentifier() + ",";
		}
		
		if (sql.length() > 0 && sql.charAt(sql.length()-1)==',') {
			sql = sql.substring(0, sql.length()-1);
		}
		
		sql += ");";
		
		return sql;
	}

	private void closeConnection(Statement stmt, ResultSet rs, Connection conn) {
		if (conn != null) {
			try {
		        conn.close();
		    } catch (SQLException sqlEx) { } // ignore

			conn = null;
		}
		
		if (rs != null) {
		    try {
		        rs.close();
		    } catch (SQLException sqlEx) { } // ignore

		    rs = null;
		}

		if (stmt != null) {
		    try {
		        stmt.close();
		    } catch (SQLException sqlEx) { } // ignore

		    stmt = null;
		}
	}
}
