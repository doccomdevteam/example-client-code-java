package database;

/**
 * Static class holding all database queries used
 */
public final class DatabaseQueries {
	private DatabaseQueries() {
		
	}
	
	/**
	 * Select query getting all patient updates that have not already been processed
	 */
	public static String TOP_50_PENDING_PATIENT_UPDATES = 
		"SELECT  PU.PatientUpdateID, "+
				"PI.Name as PatientIdentifierType, "+
				"PU.PatientIdentifierValue, "+
				"PU.DateOfBirth, "+
				"PU.DateDied, "+
				"PU.Title, "+
				"PU.FirstName, "+
				"PU.LastName, "+
				"G.Name as Gender, "+
				"PU.ClinicianID, "+
				"CL.Title as ClinicianTitle, "+
				"CL.FirstName as ClinicianFirstName, "+
				"CL.LastName as ClinicianLastName, "+
				"PU.LocationID, "+
				"LO.SiteName, "+
				"LO.AreaName, "+
				"PU.RowProcessed, "+
				"PU.TimeProcessed "+
		"FROM PatientUpdate PU "+
		"INNER JOIN PatientIdentifierType PI ON PI.PatientIdentifierTypeID = PU.PatientIdentifierTypeID "+
		"INNER JOIN Gender G ON G.GenderID = PU.GenderID "+
		"LEFT JOIN Clinician CL ON CL.ClinicianID = PU.ClinicianID "+
		"LEFT JOIN Location LO ON LO.LocationID = PU.LocationID "+
		"WHERE PU.RowProcessed = 0 "+
		"LIMIT 50 ";
}
