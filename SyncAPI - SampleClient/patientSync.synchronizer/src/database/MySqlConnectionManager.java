package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import configuration.IDatabaseConfiguration;

/**
 * Responsible for opening a connection to the MySQL database using the Connector/J drivers
 */
public class MySqlConnectionManager implements IDatabaseConnectionManager {
	private IDatabaseConfiguration databaseConfiguration;
	
	/**
	 * Initialises the connection manager
	 * @param databaseConfiguration Class responsible for getting the connection string
	 */
	public MySqlConnectionManager(IDatabaseConfiguration databaseConfiguration) {
		this.databaseConfiguration = databaseConfiguration;
	}
	
	/* (non-Javadoc)
	 * @see database.IDatabaseConnectionManager#openConnection()
	 */
	public Connection openConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String connectionString = databaseConfiguration.mySqlConnectionString();
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection conn = DriverManager.getConnection(connectionString);
		
		return conn;
	}
}
