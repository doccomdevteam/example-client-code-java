package modelTranslators;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import requestModels.PatientChangesRequest;
import schemaModels.Patient;
import schemaModels.PatientIdentifier;

/**
 * Responsible for transforming a ResultSet taken from the database into a batch request
 * ready to be sent to the API.
 */
public final class PatientChangesRequestTranslator extends AbstractPatientModelTranslator {
	private PatientChangesRequestTranslator() {
		
	}
	
	/**
	 * Translate the result set provided into a batch upload request
	 * @param rs The result set to translate
	 * @return The request object.
	 * @throws Exception 
	 */
	public static HashMap<Integer,Patient> translate(ResultSet rs) throws Exception {

		HashMap<Integer,Patient>  patientList = new  HashMap<Integer,Patient>();

		while(rs.next()) {
			Patient patient = new Patient();
			
			// Used to mark the patient as processed
			patient.setClientIdentifier(rs.getInt("PatientUpdateID"));

			buildPatient(rs, patient);
			patientList.put(patient.getClientIdentifier(), patient);
		}
		
		return patientList;
	}

	private static void buildPatient(ResultSet rs, Patient patient) throws Exception {
		//patient.setPatientIdentifiers(buildPatientIdentifier(rs));
		patient.setDateOfBirth(rs.getDate("DateOfBirth"));
		patient.setTitle(rs.getString("Title"));
		patient.setFirstName(rs.getString("FirstName"));
		patient.setLastName(rs.getString("LastName"));
		patient.setGender(getGenderType(rs));
		java.sql.Date dateDied = rs.getDate("DateDied");
		if (!rs.wasNull()) {
            patient.setDateDied(dateDied);
        }
		java.sql.Date estimatedDischargeDate = rs.getDate("EstimatedDischargeDate");
		if (!rs.wasNull()) {
			patient.setEstimatedDischargeDate(estimatedDischargeDate);
		}
		patient.setClinician(buildClinician(rs));
		patient.setLocation(buildLocation(rs));


	}

	/**
	 * Translates the result set provided into an PatientIdentifier object
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public static PatientIdentifier translateIdentifier(ResultSet rs) throws Exception {
		return buildPatientIdentifier(rs);
	}
}
