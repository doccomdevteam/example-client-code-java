package modelTranslators;

import schemaModels.Patient;
import schemaModels.PatientIdentifier;
import schemaModels.Spell;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Responsible for transforming a ResultSet taken from the database into a batch request
 * ready to be sent to the API.
 */
public final class SpellsRequestTranslator extends AbstractPatientModelTranslator {
	private SpellsRequestTranslator() {
		
	}
	
	/**
	 * Translate the result set provided into a batch upload request
	 * @param rs The result set to translate
	 * @return The request object.
	 * @throws Exception 
	 */
	public static HashMap<Integer,Spell> translate(ResultSet rs) throws Exception {

        HashMap<Integer,Spell>  spellList = new  HashMap<Integer,Spell>();

        while(rs.next()) {
			Spell spell = new Spell();

            spell.setClientIdentifier(rs.getInt("SpellID"));
			buildSpell(rs, spell);
            spellList.put(spell.getClientIdentifier(), spell);
		}
		
		return spellList;
	}

	private static void buildSpell(ResultSet rs, Spell spell) throws Exception {

		spell.setSpellIdentifier(rs.getString("SpellIdentifier"));
		java.sql.Date admissionDateTime = rs.getDate("AdmissionDateTime");
		if (!rs.wasNull()){
			spell.setAdmissionDateTime(rs.getDate("AdmissionDateTime"));
		}
		java.sql.Date dischargeDatetime = rs.getDate("DischargeDatetime");
		if (!rs.wasNull()) {
			spell.setDischargeDatetime(rs.getDate("DischargeDatetime"));
		}
		java.sql.Date utcTimestamp = rs.getDate("UtcTimestamp");
		if (!rs.wasNull()) {
			spell.setUtcTimestamp(rs.getDate("UtcTimestamp"));
		}
	}

	/**
	 * Translates the result set provided into an PatientIdentifier object
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public static PatientIdentifier translateIdentifier(ResultSet rs) throws Exception {
		return buildPatientIdentifier(rs);
	}
}
