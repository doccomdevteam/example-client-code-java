package modelTranslators;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import schemaModels.Clinician;
import schemaModels.Gender;
import schemaModels.Location;
import schemaModels.PatientIdentifier;
import schemaModels.PatientIdentifierType;

/**
 * Holds the logic for parsing the child objects within a Patient
 */
public abstract class AbstractPatientModelTranslator {

	protected static PatientIdentifier buildPatientIdentifier(ResultSet rs)
			throws Exception, SQLException {
				PatientIdentifier patientIdentifier = new PatientIdentifier();
				patientIdentifier.setPatientIdentifierType(rs.getString("Name"));
				patientIdentifier.setPatientIdentifierValue(rs.getString("PatientIdentifierValue"));

				return patientIdentifier;
			}

	protected static Clinician buildClinician(ResultSet rs) throws SQLException {
		rs.getInt("ClinicianID");
		
		if (rs.wasNull()) {
			return null;
		}
		
		Clinician clinician = new Clinician();
		
		clinician.setTitle(rs.getString("ClinicianTitle"));
		clinician.setFirstName(rs.getString("ClinicianFirstName"));
		clinician.setLastName(rs.getString("ClinicianLastName"));
		
		return clinician;
	}

	protected static Location buildLocation(ResultSet rs) throws SQLException {
		rs.getInt("LocationID");
		
		if (rs.wasNull()) {
			return null;
		}
		
		Location location = new Location();
		
		location.setSiteName(rs.getString("SiteName"));
		location.setAreaName(rs.getString("AreaName"));
		
		return location;
	}

	protected static PatientIdentifierType getIdentifierType(ResultSet rs) throws Exception {
		String patientIdentifierType = rs.getString("PatientIdentifierType");
		
		switch(patientIdentifierType)
		{
			case "NhsNumber":
				return PatientIdentifierType.NhsNumber;
			case "OrganisationIdentifier":
				return PatientIdentifierType.OrganisationIdentifier;
			default:
				throw new Exception("PatientIdentifierType " + patientIdentifierType + "not supported");
		}
	}
	
	protected static Gender getGenderType(ResultSet rs) throws Exception {
		String gender = rs.getString("Gender");
		
		switch(gender)
		{
			case "Male":
				return Gender.Male;
			case "Female":
				return Gender.Female;
			case "NotKnown":
				return Gender.NotKnown;
			case "NotSpecified":
				return Gender.NotSpecified;
			case "OtherSpecific":
				return Gender.OtherSpecific;
			default:
				throw new Exception("Gender " + gender + "not supported");
		}
	}
}
