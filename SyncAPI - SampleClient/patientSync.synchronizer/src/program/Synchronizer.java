package program;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import clients.*;
import com.google.gson.JsonArray;
import requestModels.PatientChangesRequest;
import requestModels.PatientSearchRequest;
import requestModels.SpellRequest;
import responseModels.PatientChangesResponse;
import configuration.DatabaseConfiguration;
import configuration.IDatabaseConfiguration;
import configuration.ISyncConfiguration;
import configuration.SyncConfiguration;
import database.DatabaseOperationsManager;
import database.IDatabaseConnectionManager;
import database.IDatabaseOperationManager;
import database.MySqlConnectionManager;
import responseModels.PatientSearchResponse;
import responseModels.SpellResponse;
import schemaModels.Patient;
import schemaModels.PatientIdentifier;
import schemaModels.Spell;

/**
 * Entry point for the synchronizer logic; co-ordinates the logic for pulling
 * patient updates from the database, synchronizing them with the Sync API then
 * marking them as processed.
 */
public class Synchronizer {
	/**
	 * Program entry point
	 * @param args Arguments passed to the program
	 * @throws Exception All exceptions are bubbled up to the main method
	 */
	public static void main(String [] args) {
		try {
			IDatabaseOperationManager database = getDatabaseOperationManager();

            // Uncomment the examples below you would like to run

			//RunPatientChangesEndpointExamples(database);

			RunSpellEndpointExamples(database);

			// Uncomment this to stop patients in the database being reprocessed
			//database.markPatientsAsProcessed(request);

			System.exit(0); // Success
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);  // Failure
		}
	}

	private static void RunSpellEndpointExamples(IDatabaseOperationManager database) throws Exception {
        /****** Add patient data before adding Spells ******/
        IPatientChangesClient changesClient = getPatientChangesClient();

        List<Patient> availablePatients = database.getAllPendingPatients();
        PatientChangesRequest patientChangesRequest = new PatientChangesRequest();

        for (int i = 0; i < availablePatients.size(); i++) {
            patientChangesRequest.addPatientToReplacements(availablePatients.get(i));
        }
        patientChangesRequest.setUtcTimestamp(new Date());
        PatientChangesResponse patientChangesResponse = changesClient.executeRequest(patientChangesRequest);

        if (!patientChangesResponse.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Spell Request Examples ******/
        ISpellsClient spellsClient = getSpellsClient();

        List<Spell> availableSpells = database.getAllPendingSpells();

        /****** Add Spell for an existing patient ******/
        Spell spell = availableSpells.get(0);
        ArrayList<PatientIdentifier> patientIdentifiers = spell.getPatientIdentifiers();
        ArrayList<PatientIdentifier> requestPatientIdentifiers = new ArrayList<>();
        requestPatientIdentifiers.add(patientIdentifiers.get(0));
        spell.setPatientIdentifiers(requestPatientIdentifiers);
        spell.setUtcTimestamp(new Date());
        SpellRequest request = new SpellRequest(spell);
        SpellResponse response = spellsClient.executeRequest(request);
        if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Update existing Spell for an existing patient with a new identifier ******/
        spell = availableSpells.get(0);
        spell.setPatientIdentifiers(patientIdentifiers);
        spell.setUtcTimestamp(new Date());
        request = new SpellRequest(spell);
        response = spellsClient.executeRequest(request);
        if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }
        // Check patient identifiers
        IPatientSearchClient searchClient = getPatientSearchClient();
        PatientSearchRequest patientSearchRequest = new PatientSearchRequest();
        patientSearchRequest.setIdentifierType(spell.getPatientIdentifiers().get(1).getPatientIdentifierType());
        patientSearchRequest.setIdentfierValue(spell.getPatientIdentifiers().get(1).getPatientIdentifierValue());
        PatientSearchResponse patientSearchResponse = searchClient.executeRequest(patientSearchRequest);
        if (!patientSearchResponse.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Add a Spell with two identifiers of the same type - will fail ******/
        requestPatientIdentifiers = new ArrayList<>();
        requestPatientIdentifiers.add(patientIdentifiers.get(0));
        requestPatientIdentifiers.add(availablePatients.get(1).getPatientIdentifiers().get(0));
        spell.setPatientIdentifiers(requestPatientIdentifiers);
        spell.setUtcTimestamp(new Date());
        request = new SpellRequest(spell);
        response = spellsClient.executeRequest(request);
        if (response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation, expected a failing request");
        }

        /****** Add a Spell which references two existing patients - patients will be merged ******/
        requestPatientIdentifiers = new ArrayList<>();
        requestPatientIdentifiers.add(patientIdentifiers.get(0));
        requestPatientIdentifiers.add(availablePatients.get(2).getPatientIdentifiers().get(0));
        spell.setPatientIdentifiers(requestPatientIdentifiers);
        spell.setUtcTimestamp(new Date());
        request = new SpellRequest(spell);
        response = spellsClient.executeRequest(request);
        if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Add a Spell which references a patient which is not associated to the spell and the spell also has an associated patient - patients will be merged ******/
        requestPatientIdentifiers = new ArrayList<>();
        requestPatientIdentifiers.add(patientIdentifiers.get(1));
        spell.setPatientIdentifiers(requestPatientIdentifiers);
        spell.setUtcTimestamp(new Date());
        request = new SpellRequest(spell);
        response = spellsClient.executeRequest(request);
        if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Delete all patients Cleanup - All 3 patients should have merged into 1 so we only need to use 1 identifier ******/
        patientChangesRequest =  new PatientChangesRequest();
        patientChangesRequest.addPatientToDeletions(availablePatients.get(2));
        patientChangesRequest.setUtcTimestamp(new Date());
        patientChangesResponse = changesClient.executeRequest(patientChangesRequest);

        if (!patientChangesResponse.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }
    }

	private static void RunPatientChangesEndpointExamples(IDatabaseOperationManager database) throws Exception {
		IPatientChangesClient changesClient = getPatientChangesClient();

		List<Patient> availablePatients = database.getAllPendingPatients();
		PatientChangesRequest request = new PatientChangesRequest();

		/****** Adding new patients ******/
		for (int i = 0; i < availablePatients.size(); i++) {
            request.addPatientToReplacements(availablePatients.get(i));
        }
		request.setUtcTimestamp(new Date());
		PatientChangesResponse response = changesClient.executeRequest(request);

		if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Check patients were added correctly ******/
		IPatientSearchClient searchClient = getPatientSearchClient();
		PatientSearchRequest patientSearchRequest = new PatientSearchRequest();
		patientSearchRequest.setIdentifierType("CustomIdentifier");
		patientSearchRequest.setIdentfierValue("CustomIDForPatient");
		PatientSearchResponse patientSearchResponse = searchClient.executeRequest(patientSearchRequest);
		if (!patientSearchResponse.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Delete all added patients ******/
		request =  new PatientChangesRequest();
		for (int i = 0; i < availablePatients.size(); i++) {
            request.addPatientToDeletions(availablePatients.get(i));
        }
		request.setUtcTimestamp(new Date());
		response = changesClient.executeRequest(request);

		if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Check patients were deleted correctly ******/
		searchClient = getPatientSearchClient();
		patientSearchRequest = new PatientSearchRequest();
		patientSearchRequest.setIdentifierType("CustomIdentifier");
		patientSearchRequest.setIdentfierValue("CustomIDForPatient");
		patientSearchResponse = searchClient.executeRequest(patientSearchRequest);
		JsonArray responseData = patientSearchResponse.getBody();
		if (!patientSearchResponse.isSuccessful() && responseData.size() == 0)
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Update & Delete existing patients ******/
		request = new PatientChangesRequest();
		// Add the first patient without any changes
		request.addPatientToReplacements(availablePatients.get(0));

		// Add a new patientidentifier for the last patient
		Patient patientToAdd = availablePatients.get(2);
		PatientIdentifier patientIdentifier = new PatientIdentifier();
		patientIdentifier.setPatientIdentifierType("BloodType");
		patientIdentifier.setPatientIdentifierValue("A");
		patientToAdd.setPatientIdentifiers(patientIdentifier);
		request.addPatientToReplacements(patientToAdd);

		// Delete the second patient
		request.addPatientToDeletions(availablePatients.get(1));

		request.setUtcTimestamp(new Date());
		response = changesClient.executeRequest(request);

		if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Check patient 3 new identifier was added correctly ******/
		searchClient = getPatientSearchClient();
		patientSearchRequest = new PatientSearchRequest();
		patientSearchRequest.setIdentifierType("BloodType");
		patientSearchRequest.setIdentfierValue("A");
		patientSearchResponse = searchClient.executeRequest(patientSearchRequest);
		responseData = patientSearchResponse.getBody();
		if (!patientSearchResponse.isSuccessful() && responseData.size() != 1)
        {
            throw new Exception("Request failed, aborting operation");
        }

		/****** Check patient 2 was removed correctly ******/
		searchClient = getPatientSearchClient();
		patientSearchRequest = new PatientSearchRequest();
		patientSearchRequest.setIdentifierType("NhsNumber");
		patientSearchRequest.setIdentfierValue("4010232331");
		patientSearchResponse = searchClient.executeRequest(patientSearchRequest);
		responseData = patientSearchResponse.getBody();
		if (!patientSearchResponse.isSuccessful() && responseData.size() != 0)
        {
            throw new Exception("Request failed, aborting operation");
        }

        /****** Delete all patients Cleanup ******/
        request =  new PatientChangesRequest();
        for (int i = 0; i < availablePatients.size(); i++) {
            request.addPatientToDeletions(availablePatients.get(i));
        }
        request.setUtcTimestamp(new Date());
        response = changesClient.executeRequest(request);

        if (!response.isSuccessful())
        {
            throw new Exception("Request failed, aborting operation");
        }
	}

	private static IDatabaseOperationManager getDatabaseOperationManager() throws FileNotFoundException, IOException {
		IDatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
		
		IDatabaseConnectionManager databaseConnectionManager = 
				new MySqlConnectionManager(databaseConfiguration);
		
		return new DatabaseOperationsManager(databaseConnectionManager);
	}

    private static ISpellsClient getSpellsClient() throws Exception {
        ISyncConfiguration syncConfig = new SyncConfiguration();
        IAuthenticationClient authClient = new AuthenticationClient(syncConfig);

        return new SpellsClient(syncConfig, authClient);
    }

	private static IPatientChangesClient getPatientChangesClient() throws Exception {
		ISyncConfiguration syncConfig = new SyncConfiguration();
		IAuthenticationClient authClient = new AuthenticationClient(syncConfig);
		
		return new PatientChangesClient(syncConfig, authClient);
	}

	private static IPatientSearchClient getPatientSearchClient() throws Exception {
		ISyncConfiguration syncConfig = new SyncConfiguration();
		IAuthenticationClient authClient = new AuthenticationClient(syncConfig);

		return new PatientSearchClient(syncConfig, authClient);
	}
}
