package configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Responsible for pulling configuration information out of the config.properties file
 */
public class DatabaseConfiguration implements IDatabaseConfiguration {
	private static String MYSQL_CONNECTION_STRING_KEY = "mysqlconnectionstring";
	
	private ResourceBundle prop;
	
	/**
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public DatabaseConfiguration() throws FileNotFoundException, IOException {
		prop = ResourceBundle.getBundle("config");
	}
	
	/* (non-Javadoc)
	 * @see configuration.IDatabaseConfiguration#mySqlConnectionString()
	 */
	public String mySqlConnectionString() {
		return prop.getString(MYSQL_CONNECTION_STRING_KEY);
	}
}
