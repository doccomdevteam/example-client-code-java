package configuration;

/**
 * Responsible for pulling configuration information out of the config.properties file
 */
public interface IDatabaseConfiguration {
	
	/**
	 * The connection string to the MySQL database.
	 * @return The MySQL connection string
	 */
	public String mySqlConnectionString();
}