# SyncAPI

Sample code for Careflow's SyncAPI endpoint on the Population Service.

Documentation can be found in the [developer portal](http://careflowdeveloperportal.azurewebsites.net/documentation/syncapi/endpoints). To request access email [support@careflowconnect.com](mailto:support@careflowconnect.com).

# Download Virtual Hard Drive 

In case a Virtual Hard Drive with a pre-integrated environment ready for development is needed, it can be downloaded from [here](https://cfassets.blob.core.windows.net/publicvhds/JavaApiClientVirtualMachine.vhd).

User: javaapiclient

Password: CareflowConnect